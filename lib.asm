global exit
global string_length
global print_string
global print_char
global print_uint
global print_newline
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_err
global print_error_string
global read_string

%define stdout 1
%define stderr 2

section .data

err: db "No such key in a dictionary!",0

section .text 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length
	mov     rsi, rdi
	mov     rdx, rax
	mov     rax, 1
	mov     rdi, 1
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    mov rax, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rax, 0xA
    push rax
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    mov rax, 1
    syscall
    pop rax
    ret
    
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	xor rax, rax
	xor rcx, rcx
	mov r10, 0xA
	dec rsp
	mov [rsp], al
	mov rax, rdi
.loop:
	xor rdx, rdx
	div r10
	add dl, 0x30
	dec rsp
	mov [rsp], dl
	inc rcx
	cmp rax, 0
	jne .loop
.print:
	mov rdi, rsp
	push rcx
	call print_string
	pop rcx
	add rsp, rcx
	inc rsp
	ret

; Выводит знаковое 8-байтовое число в десятичном формате (stored in rdi)
print_int:
    xor rax, rax
    mov rax, rdi
    mov r10, 0xA
    xor rcx, rcx
.sign:
	cmp rax, 0
	jge .number
	mov rdi, '-'
	push rax
	call print_char
	pop rax
	neg rax
.number:
	mov rdi, rax
	call print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r8, r8
    xor r9, r9
.loop:
    lea r8, [rdi + rax]
    lea r9, [rsi + rax]
    mov r8b, byte[r8]
    cmp r8b, byte[r9]
    jne .no
    cmp byte[r9], 0
    je .yes
    inc rax
    jmp .loop
.yes:
    mov rax, 1
    ret
.no:
    mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера (rdi), размер буфера (rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor r8, r8 ; curr pointer
    xor r9, r9 ; curr shift & curr len without null-terminator
    xor r10, r10 ; flag, zero before first nonblank symb
.elem:
	push rsi
	push rdi
    call read_char ; symbol in rax
	pop rdi
	pop rsi
    cmp al, 0x20
    je .blank
    cmp al, 0x9
    je .blank
    cmp al, 0xA
    je .blank
    cmp r10b, 0 ; if u a here that means current symb is not 20 A or 9 , lets find out if it is the first one
    jne .store
    mov r10b, 1 ; flag that str begins
.store:
    cmp rsi, r9 ; buff size & curr shift
    jle .fail
    lea r8, [rdi + r9]
    mov [r8], rax
    cmp al, 0
    je .success
    inc r9
    jmp .elem
.fail:
    mov rax, 0  
    ret
.success:
    mov rax, rdi
    mov rdx, r9
    ret
.blank:
	cmp r10b, 0
	je .elem
	xor rax, rax
	jmp .store
 
; buff size in rsi, buff pointer in rdi, success will be stored in rax
read_string:
	xor r8, r8 ; symb counter
.read_symb:
	cmp r8, rsi
	je .fail
	push rsi
	push rdi
    call read_char ; symbol in rax
	pop rdi
	pop rsi
	cmp rax, 0
	je .success
	cmp rax, 0xA
	je .success
	mov byte[rdi+r8], al
	inc r8
	jmp .read_symb
.success:
	mov byte[rdi+r8], 0
	xor rax, rax
	ret
.fail:
	mov rax, 1
	ret

; Принимает указатель на строку (rdi), пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:
	xor rax, rax
	xor rdx, rdx
	mov r9, 10
	lea rdi, [rdi]
.loop:
	cmp byte [rdi], '0'
	jb .end
	cmp byte [rdi], '9'
	ja .end
	push rdx
	mul r9 ; uses rdx
	pop rdx
	add al, byte [rdi]
	sub al, 0x30
	inc rdx
	inc rdi
	lea rdi, [rdi]
	jmp .loop
.end:
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor r9, r9
    xor r10, r10 ; sign flag 0=plus
.sign:
	cmp byte [rdi], '-'
	je .neg
	cmp byte [rdi], '+'
	je .pos
	jmp .unsig
.neg:
	inc r10
.pos:
	inc rdi
	inc r9
.unsig:
	push r9
    push rdi
    call parse_uint ; num in rax
    pop rdi
    pop r9
	cmp r10, 0
	je .end
	neg rax
.end:
	add rdx, r9
    ret 

; Принимает указатель на строку (rdi), указатель на буфер (rsi) и длину буфера (rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r10, r10 ; current str length
.loop:
    cmp rdx, 0
    jle .zero_res
    lea rdi, [rdi + r10]
    lea rsi, [rsi + r10]
    mov rax, [rdi]
    mov [rsi], rax
    cmp byte [rdi], 0
    je .res_length
    dec rdx
    inc r10
    jmp .loop
.zero_res:
    mov rax, 0
    ret
.res_length:
    mov rax, r10
    ret

print_err:
	mov rdi, err
	call print_error_string
	ret

; string pointer in rdi
print_error_string:
	call string_length
	mov     rsi, rdi
	mov     rdx, rax
	mov     rax, 1
	mov     rdi, stderr
	syscall
	ret
