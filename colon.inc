%ifndef LAST_VAL
	%define LAST_VAL 0
%endif

%ifndef KEY_OFFSET
	%define KEY_OFFSET 8
%endif

%macro colon 2

%ifid %2
%ifstr %1

%%entry_start:
	dq LAST_VAL
	db %1, 0
%2:

%define LAST_VAL %%entry_start

%else
	%error "Invalid args"
%endif
%endif

%endmacro

