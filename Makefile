prog: lib.o dict.o main.o
	ld -o prog main.o lib.o dict.o

lib.o: lib.asm
	nasm -f elf64 -o lib.o lib.asm

dict.o: dict.asm lib.o
	nasm -f elf64 -o dict.o dict.asm

main.o: main.asm dict.o lib.o words.inc colon.inc
	nasm -f elf64 -o main.o main.asm
	
clean:
	rm -f prog *.o
