%include "words.inc"
%include "lib.inc"

global _start
extern find_word

%define BUFFER_SIZE 256

section .data
err1: db "No such key in a dictionary!", 0
err2: db "Buffer overflow!", 0

section .text

_start:
	push rbp
	mov rbp, rsp
	sub rsp, BUFFER_SIZE
	mov rsi, BUFFER_SIZE
	mov rdi, rsp
	call read_string
	cmp rax, 0
	jne .read_string_error
.find:
	mov rdi, rsp
	mov rsi, LAST_VAL
	call find_word
	cmp rax, 0
	je .key_error
	mov rdi, rax
	add rdi, KEY_OFFSET
	call string_length
	add rdi, rax
	inc rdi
	call print_string
	xor rdi, rdi
	jmp .end
.key_error:
	mov rdi, err1
	call print_error_string
	mov rdi, 1
	jmp .end
.read_string_error:
	mov rdi, err2
	call print_error_string
	mov rdi, 1
.end:
	mov rsp, rbp
	pop rbp
	call exit
