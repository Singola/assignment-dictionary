%include "lib.inc"

global find_word

section .text

; rdi - str pointer
; rsi - dict pointer
; ret entry addr if found and 0 if not

find_word:
	cmp rsi, 0
	je .entry_not_found
	push rdi
	push rsi
	add rsi, 8 ; key str is after pointer, pointer is dq = 8 bytes
	call string_equals ; 1 if equal in rax
	pop rsi
	pop rdi
	cmp rax, 1
	je .entry_found ; entry start in rsi
	mov rsi, [rsi]
	jmp find_word
.entry_found:
	mov rax, rsi
	ret
.entry_not_found:
	xor rax, rax
	ret
